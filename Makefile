
SRC_DIR := ./src

BUILD_FOLDER := build

CPP := g++

DEBUG_FLAG = -g

OUTPUT_FLAG = -o

BUILD_DIR := ./${BUILD_FOLDER}

SRCS := $(shell find $(SRC_DIR) -name *.cpp -or -name *.c)

SRC_HEADERS := $(shell find $(SRC_DIR) -name *.h)

## Find the path of the source files
SRC_PATHS := $(dir ${SRCS})

## Find the relative path of each source file
REL_PATH := $(SRC_PATHS:./src/%=%)
REL_PATH := $(REL_PATH:%/=%)

TEMP_PATH := $(subst ${REL_PATH}/,, ${SRCS})
OBJ_PATH := $(subst ./src,./build,${TEMP_PATH})

# Rename the source files found to object files for
# placing in the build folder
OBJS := $(patsubst %.cpp, %.o, ${OBJ_PATH})
OBJS := $(patsubst %.c, %.o, ${OBJS})

# Prepare the header files 
SRC_HDRS_PATH := $(dir ${SRC_HEADERS})
SRC_HDRS := $(subst ./,-I, ${SRC_HDRS_PATH})



test_com:
	@echo ${SRC_HDRS}

build_folder_creation:
	@if [ -d ${BUILD_FOLDER} ] ; then echo "Build folder already exists!!" ; else mkdir ${BUILD_FOLDER} ; fi


object_naming: build_file_creation
	@echo ${OBJS} 



source_header_serach:
	@echo ${SRC_HEADERS}

source_header_prepare: 
	@echo ${SRC_HDRS}

build_project: source_header_serach source_header_prepare
	${CPP} ${DEBUG_FLAG} ${OUTPUT_FLAG} ${BUILD_FOLDER}/main ${SRCS} ${SRC_HDRS} 


